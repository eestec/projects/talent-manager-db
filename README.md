# Talent Manager Database

_Dear Talent Manager,_

_Please READ carefully the following instructions in order to understand how your Databse works!_

---

1. Browse to [db.eestec.net](https://db.eestec.net) 

2. Log In with your credentials 

3. On the top left corner, select _Servers_ --> _Talent DB_ --> _Databases_ --> _talentdb_

4. From the top menu bar select _Tools_ --> _Query Tools_

In the new blanc window (_Query Editor_) you can write your SQL Queries to get results from the Database.
<br><br>
To execute the queries **Press F5** or the button with the lightning in the middle of your screen.
<br>

- **Option 1.** Show all people in the DB
<br>    `SELECT * FROM person;`
<br>

- **Option 2.** Show all positions in the DB
<br>    `SELECT * FROM position;`
<br>

- **Option 3.** Show all skills in the DB
<br>    `SELECT * FROM skill;`
<br>

- **Option 4.** Show all matches in the DB
<br>    `SELECT * FROM matching;`
<br>

- **Option 5.** Show all ratings of a certain position  (**HINT:** ex. `SELECT * FROM position_skills('Talent Manager');`)
<br>    `SELECT * FROM position_skills('{your position}');`
<br>

- **Option 6.** Show all ratings of a certain person    (**HINT:** ex. `SELECT * FROM personal_skills('talent_manager@eestec.net');`)
<br>    `SELECT * FROM personal_skills('{your email}');`
<br>

- **Option 7.** Show the skills that people possess the least   (**HINT:** choose between `'hard_skill'`, `'soft_skill'`, `'personality_trait'`)
<br>    `SELECT * FROM worst_skills('{your skill}');` 
<br>

- **Option 8.** Show people that are suited for a certain position  (**HINT:** ex. `SELECT * FROM position_matching('Talent Manager');`)
<br>    `SELECT * FROM position_matching('{ your position }');`
<br>

---

**®EESTEC IT Team 2021**

*George Glarakis - Jovan Dojcilovic - Utku Karadeniz - Giannis Kalis*
