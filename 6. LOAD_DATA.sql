-- Step 10. Insert personal info into Person table

BEGIN TRANSACTION;

INSERT INTO Person (fullname, email, branch, year_of_studies, level_of_degree, last_mandate) 
    (SELECT fullname, email, branch, year_of_studies, level_of_degree, '2020-2021'
        FROM peoples_answers);

SELECT * FROM Person;

--COMMIT --ROLLBACK

-- Step 11. Execute IMPORT functions

BEGIN TRANSACTION;

SELECT import_people();
SELECT * FROM matching;

SELECT import_positions();
SELECT COUNT(*) FROM matching;

--COMMIT --ROLLBACK

-- Step 12. Test the functions

SELECT * FROM person;
SELECT * FROM position;
SELECT * FROM skill;
SELECT * FROM matching;

SELECT * FROM position_skills('Ambassador');
SELECT * FROM personal_skills('sel.eestec@gmail.com');
SELECT * FROM worst_skills('hard_skill');
SELECT * FROM position_matching('Ambassador');